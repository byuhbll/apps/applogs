#!/bin/bash -e

# see /etc/bash_completion.d/log for tab completion

usage() {
  cat <<EOF
Usage: $(basename "${BASH_SOURCE[0]}") [OPTIONS] COMMAND APPNAME

Interact with application log files.

Options:
  -h, --help  Print this help and exit
  -p          Use production environment (default)
  -r          Use review environment
  -s          Use staging environment

Commands:
  follow, f   follow the log
  search, s   search the log
  details, d  print details about the log

Application name:
  Application name as found in GitLab. Append /{BRANCH_NAME} for review apps.
EOF
  exit
}

follow() {
  tail -F -n 200 "/var/log/app/$environment/$1/"*.log
}

search() {
  zgrep -i "$2" `ls -t "/var/log/app/$environment/$1/"*.*log*`
}

details() {
  dir="/var/log/app/$environment/$1"
  du -sh "$dir"
  entry_count=`ls "$dir" | wc -l`

  if [ $entry_count -lt 20 ]; then
    ls -lt "$dir" | grep .log
  else
    ls -lt "$dir" | head -n 11| grep .log
    echo "..."
    ls -hlt "$dir" | tail -n 10| grep .log
  fi
}

# read the options
TEMP=`getopt -o hprs --long help -- "$@"`
eval set -- "$TEMP"

# default values
environment=production

# extract options and their arguments into variables.
while true ; do
  case "$1" in
    -h|--help) usage ;;
    -p) environment="production"; shift 1 ;;
    -r) environment="review"; shift 1 ;;
    -s) environment="staging"; shift 1 ;;
    --) shift ; break ;;
    *) echo "Internal error!" ; exit 1 ;;
  esac
done

case "$1" in
  follow|f) shift; follow "$@" ;;
  search|s) shift; search "$@" ;;
  details|d) shift; details "$@" ;;
  *) usage ;;
esac
