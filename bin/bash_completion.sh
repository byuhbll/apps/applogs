#/usr/bin/env bash

_log_completions() {
  WORD=${COMP_WORDS[$COMP_CWORD]}
  PRIOR_CWORD=$(($COMP_CWORD - 1))
  PRIOR_WORD=${COMP_WORDS[$PRIOR_CWORD]}

  TEMP=`getopt -o hprs --long help -- "${COMP_WORDS[@]:1:$COMP_CWORD}"`
  eval set -- "$TEMP"

  # default values
  environment=production

  # extract options and their arguments into variables.
  while true ; do
    case "$1" in
      -h|--help) shift 1;;
      -p) environment="production"; shift 1 ;;
      -r) environment="review"; shift 1 ;;
      -s) environment="staging"; shift 1 ;;
      --) shift; break ;;
      *) echo "Internal error!" ; exit 1 ;;
    esac
  done

  if [ "$#" == 1 ]; then
    COMPREPLY=($(compgen -W "follow search details" -- "$WORD"))
  elif [ "$#" == 2 ]; then
    if [ "$environment" = "review" ]; then
      COMPREPLY=($(compgen -W "$(cd /var/log/app/review && find * -maxdepth 1 -mindepth 1 -type d)" -- "$WORD"))
    else
      COMPREPLY=($(compgen -W "$(ls /var/log/app/$environment)" -- "$WORD"))
    fi
  fi
}

complete -F _log_completions log