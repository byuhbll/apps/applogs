package edu.byu.hbll.applogs;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.rolling.RollingFileAppender;
import ch.qos.logback.core.rolling.SizeAndTimeBasedRollingPolicy;
import ch.qos.logback.core.util.FileSize;
import java.io.File;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

/**
 * Prints logs to rotating log files. Logs are rotated daily, unless the logs grow too big at which
 * point they're rotated more often. Log names are based on GitLab's production, staging, and review
 * environments.
 *
 * <p>Configuration is done programmatically or via environment variables:
 *
 * <ul>
 *   <li>FILE_BASE_LOG_DIR: Location to write the log files (default: ./applogs)
 *   <li>FILE_MAX_FILE_SIZE: Maximum individual log file size (default: 100MB)
 *   <li>FILE_TOTAL_SIZE_CAP: Cap for total size of logs (default: 1G)
 *   <li>FILE_MAX_HISTORY: Number of days of logs (default: 90)
 *   <li>FILE_ENABLED: Whether this logger is enabled (default: true)
 * </ul>
 */
@Component
public class FileLogger implements Consumer<Event> {

  private Map<String, Logger> loggers = new ConcurrentHashMap<>();

  private final String baseLogDir;
  private final String maxFileSize;
  private final String totalSizeCap;
  private final int maxHistory;
  private final boolean enabled;

  private FileLogger(Properties properties) {
    this.baseLogDir = properties.baseLogDir;
    this.maxFileSize = properties.maxFileSize;
    this.totalSizeCap = properties.totalSizeCap;
    this.maxHistory = properties.maxHistory;
    this.enabled = properties.enabled;
  }

  @Override
  public void accept(Event event) {
    if (!enabled) {
      return;
    }

    Logger logger = loggers.computeIfAbsent(logName(event), k -> createLogger(k));

    logger.info(event.getRawMessage());
  }

  /**
   * Creates a log name out of the podName and namespace.
   *
   * @param podName the podName
   * @param namespace the namespace
   * @return an appropriate log name given the environment
   */
  private String logName(Event event) {
    if (event.getBranchName() != null) {
      return event.getAppName() + "/" + event.getBranchName() + "/" + event.getAppName();
    } else if (event.getAppName() != null) {
      return event.getAppName() + "/" + event.getAppName();
    } else {
      return event.getNamespace() + "/" + event.getSimplePodName();
    }
  }

  /**
   * Creates a new logger for the given log name and writes to paths of the same name.
   *
   * @param logName the unique log name for this log.
   * @return the newly created logger
   */
  private Logger createLogger(String logName) {
    LoggerContext context = (LoggerContext) LoggerFactory.getILoggerFactory();
    String logPath = new File(baseLogDir, logName).toString();

    // appender
    RollingFileAppender<ILoggingEvent> appender = new RollingFileAppender<ILoggingEvent>();
    appender.setFile(logPath + ".log");
    appender.setContext(context);

    // encoder
    PatternLayoutEncoder encoder = new PatternLayoutEncoder();
    encoder.setContext(context);
    encoder.setPattern("%m%n");

    // policy
    SizeAndTimeBasedRollingPolicy<ILoggingEvent> policy = new SizeAndTimeBasedRollingPolicy<>();
    policy.setContext(context);
    policy.setFileNamePattern(logPath + ".%d{yyyyMMdd}.%i.log.gz");
    policy.setMaxHistory(maxHistory);
    policy.setTotalSizeCap(FileSize.valueOf(totalSizeCap));
    policy.setMaxFileSize(FileSize.valueOf(maxFileSize));
    policy.setCleanHistoryOnStart(true);

    // connect
    appender.setEncoder(encoder);
    appender.setRollingPolicy(policy);
    policy.setParent(appender);

    // start (order is important)
    policy.start();
    encoder.start();
    appender.start();

    // logger
    ch.qos.logback.classic.Logger logger =
        (ch.qos.logback.classic.Logger) LoggerFactory.getLogger(logName);
    logger.addAppender(appender);
    logger.setLevel(Level.INFO);
    logger.setAdditive(false);

    return logger;
  }

  /**
   * Whether or not this logger is enabled.
   *
   * @return whether or not this logger is enabled.
   */
  public boolean isEnabled() {
    return enabled;
  }

  /** {@link FileLogger} properties. */
  @Validated
  @Configuration
  @ConfigurationProperties(prefix = "file")
  @Data
  public static class Properties {
    private String baseLogDir = "./applogs";
    private String maxFileSize = "100MB";
    private String totalSizeCap = "1GB";
    private Integer maxHistory = 90;
    private Boolean enabled = true;
  }
}
