package edu.byu.hbll.applogs;

import com.fasterxml.jackson.databind.JsonNode;
import java.time.Instant;
import lombok.Data;

/** A log event. */
@Data
public class Event {
  private JsonNode json;
  private String podName;
  private String namespace;
  private String simplePodName;
  private String appName;
  private String branchName;
  private String projectName;
  private String rawMessage;
  private Instant timestamp;
  private Level level;
  private String message;

  /**
   * Returns true if this event is the first line of a single or multi-line event.
   *
   * @return true if this event is the first line of a single or multi-line event
   */
  public boolean isFirstLine() {
    return timestamp != null;
  }
}
