package edu.byu.hbll.applogs;

import com.fasterxml.jackson.databind.JsonNode;
import edu.byu.hbll.json.ObjectMapperFactory;
import edu.byu.hbll.json.UncheckedObjectMapper;
import io.sentry.Hub;
import io.sentry.SentryEvent;
import io.sentry.SentryLevel;
import io.sentry.SentryOptions;
import io.sentry.protocol.Message;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.net.URI;
import java.net.URLEncoder;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpRequest.BodyPublishers;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * Sends ERROR log events to Sentry.
 *
 * <p>Configuration is done programmatically or via environment variables:
 *
 * <ul>
 *   <li>SENTRY_BASE_URI: The base URI of the Sentry instance (default: https://sentry.io/)
 *   <li>SENTRY_ORGANIZATION: Your Sentry organization (required)
 *   <li>SENTRY_TEAM: Your Sentry team (required)
 *   <li>SENTRY_ENVIRONMENT: The environment (default: production)
 *   <li>SENTRY_API_TOKEN: Sentry API token (required)
 *   <li>SENTRY_ENABLED: Whether this logger is enabled (default: false)
 *   <li>SENTRY_EVENT_CAP: Max number of events to send per period per error (default: 100)
 *   <li>SENTRY_CAP_RESET_SCHEDULE: Schedule to reset event counter (default: 0 0 * * * * *
 *       (hourly))
 *   <li>SENTRY_RULES: Array of alert rules (default: see application.yml)
 * </ul>
 */
@Component
public class SentryLogger implements Consumer<Event>, AutoCloseable {

  private static final int MAX_FINGERPRINT_SIZE = 20;

  private static final UncheckedObjectMapper mapper = ObjectMapperFactory.newUnchecked();
  private static final HttpClient client = HttpClient.newHttpClient();
  private ConcurrentHashMap<String, Hub> sentryHubs = new ConcurrentHashMap<>();
  private Map<String, Breaker> breakers = new ConcurrentHashMap<>();

  private final URI baseUri;
  private final String organization;
  private final String team;
  private final String environment;
  private final String apiToken;
  private final boolean enabled;
  private final int eventCap;
  private final List<Rule> rules;

  /**
   * Creates a new {@link SentryLogger}.
   *
   * @param properties the application properties
   */
  public SentryLogger(Properties properties) {
    this.baseUri =
        URI.create(properties.baseUri + (properties.baseUri.getPath().endsWith("/") ? "" : "/"));
    this.organization = properties.organization;
    this.team = properties.team;
    this.environment = properties.environment;
    this.apiToken = properties.apiToken;
    this.enabled = properties.enabled;
    this.eventCap = properties.eventCap;
    this.rules = properties.rules;
  }

  @Override
  public void accept(Event event) {
    if (!enabled) {
      return;
    }

    if (event.getLevel() == Level.FATAL || event.getLevel() == Level.ERROR) {
      String projectSlug = event.getAppName();
      List<String> fingerPrint = createFingerprint(event.getMessage());
      Breaker breaker = breakers.computeIfAbsent(projectSlug + fingerPrint, k -> new Breaker());

      if (breaker.increment()) {
        SentryEvent sentryEvent = new SentryEvent(Date.from(event.getTimestamp()));
        Message message = new Message();
        message.setFormatted(event.getMessage());
        sentryEvent.setMessage(message);
        sentryEvent.setLevel(SentryLevel.ERROR);
        // zero out default threads from this application
        sentryEvent.setThreads(List.of());
        sentryEvent.setFingerprints(fingerPrint);

        Hub hub = sentryHubs.computeIfAbsent(projectSlug, k -> initHub(projectSlug));
        hub.captureEvent(sentryEvent);
      }
    }
  }

  /**
   * Initializes the {@link Hub} (or Sentry client) per project slug.
   *
   * @param projectSlug the project slug
   * @return the initialized hub
   */
  private Hub initHub(String projectSlug) {
    String dsn = getDsn(projectSlug);

    if (dsn == null) {
      createProject(projectSlug);
      dsn = getDsn(projectSlug);
    }

    upsertRules(projectSlug);

    SentryOptions options = new SentryOptions();
    options.setEnvironment(environment);
    options.setDsn(dsn);
    Hub hub = new Hub(options);
    return hub;
  }

  /**
   * Get the DSN (Sentry secret key) for the project.
   *
   * @param projectSlug the project slug
   * @return the DSN
   */
  private String getDsn(String projectSlug) {
    HttpRequest request =
        HttpRequest.newBuilder()
            .uri(
                URI.create(
                    baseUri
                        + "api/0/projects/"
                        + encode(organization)
                        + "/"
                        + encode(projectSlug)
                        + "/keys/"))
            .header("Content-Type", "application/json")
            .header("Authorization", "Bearer " + apiToken)
            .GET()
            .build();

    HttpResponse<String> response;

    try {
      response = client.send(request, BodyHandlers.ofString());
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    } catch (InterruptedException e) {
      return null;
    }

    if (response.statusCode() == 404) {
      return null;
    }

    JsonNode json = mapper.readTree(response.body());

    return json.path(0).path("dsn").path("public").asText(null);
  }

  /**
   * Creates a Sentry project given the project slug.
   *
   * @param projectSlug the project slug
   */
  private void createProject(String projectSlug) {
    String payload =
        mapper.createObjectNode().put("name", projectSlug).put("slug", projectSlug).toString();

    HttpRequest request =
        HttpRequest.newBuilder()
            .uri(
                URI.create(
                    baseUri
                        + "api/0/teams/"
                        + encode(organization)
                        + "/"
                        + encode(team)
                        + "/projects/"))
            .header("Content-Type", "application/json")
            .header("Authorization", "Bearer " + apiToken)
            .POST(BodyPublishers.ofString(payload))
            .build();

    try {
      client.send(request, HttpResponse.BodyHandlers.ofString());
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    } catch (InterruptedException e) {
      return;
    }
  }

  /**
   * Retrieves the ids by name for the rules associated with the given project.
   *
   * @param projectSlug the project slug
   */
  private Map<String, String> getRuleIds(String projectSlug) {
    HttpRequest request =
        HttpRequest.newBuilder()
            .uri(
                URI.create(
                    baseUri
                        + "api/0/projects/"
                        + encode(organization)
                        + "/"
                        + encode(projectSlug)
                        + "/rules/"))
            .header("Content-Type", "application/json")
            .header("Authorization", "Bearer " + apiToken)
            .GET()
            .build();

    HttpResponse<String> response;

    try {
      response = client.send(request, BodyHandlers.ofString());
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    } catch (InterruptedException e) {
      return null;
    }

    Map<String, String> ruleIds = new HashMap<>();

    for (JsonNode rule : mapper.readTree(response.body())) {
      ruleIds.put(rule.path("name").asText(""), rule.path("id").asText());
    }

    return ruleIds;
  }

  /**
   * Creates or updates alert rules for the given project.
   *
   * @param projectSlug the project slug
   */
  private void upsertRules(String projectSlug) {
    Map<String, String> ruleIds = getRuleIds(projectSlug);

    for (Rule rule : rules) {
      String ruleId = ruleIds.get(rule.getName());
      String payload = mapper.writeValueAsString(rule);

      HttpRequest request =
          HttpRequest.newBuilder()
              .uri(
                  URI.create(
                      baseUri
                          + "api/0/projects/"
                          + encode(organization)
                          + "/"
                          + encode(projectSlug)
                          + "/rules/"
                          + (ruleId != null ? ruleId + "/" : "")))
              .header("Content-Type", "application/json")
              .header("Authorization", "Bearer " + apiToken)
              .method(ruleId != null ? "PUT" : "POST", BodyPublishers.ofString(payload))
              .build();

      try {
        client.send(request, HttpResponse.BodyHandlers.ofString());
      } catch (IOException e) {
        throw new UncheckedIOException(e);
      } catch (InterruptedException e) {
        return;
      }
    }
  }

  /**
   * URL encodes the given value.
   *
   * @param value the value to encode
   * @return the encoded value
   */
  private String encode(String value) {
    return URLEncoder.encode(value, StandardCharsets.UTF_8);
  }

  /**
   * The fingerprint determines how issues merge together. The default strategy on Sentry's side is
   * to compare stack traces. That's difficult because the stack trace is just part of the message.
   * Here we do something similar.
   *
   * @param message the error message
   * @return the fingerprint
   */
  private List<String> createFingerprint(String message) {
    List<String> fingerprint = new ArrayList<>();

    for (String line : message.split("\n")) {
      // remove unique id from spring beans
      line = line.replaceAll("\\$\\$[0-9a-f]+(\\W)", "$1");

      if (line.startsWith("\tat")) {
        // java stacktrace frame
        fingerprint.add(line);
      } else if (line.startsWith("Caused by: ")) {
        // inner java exception
        fingerprint.add(line.replaceFirst("^Caused by: ([^:]+).*", "$1"));
      }
    }

    // if unable to extract fingerprint, group by date for now
    if (fingerprint.isEmpty()) {
      fingerprint.add(LocalDate.now().toString());
    }

    // limit fingerprint to top MAX_FINGERPRINT_SIZE lines to group more aggressively
    fingerprint = fingerprint.stream().limit(MAX_FINGERPRINT_SIZE).toList();

    return fingerprint;
  }

  /**
   * Clear breakers according to reset schedule. Note that clearing them rather than resetting them
   * is important so the collection doesn't grow unbounded.
   */
  @Scheduled(cron = "${sentry.cap-reset-schedule:0 0 * * * *}")
  public void clearBreakers() {
    breakers.clear();
  }

  @Override
  public void close() {
    sentryHubs.values().forEach(h -> h.flush(TimeUnit.SECONDS.toMillis(10)));
  }

  /**
   * Whether or not this logger is enabled.
   *
   * @return whether or not this logger is enabled.
   */
  public boolean isEnabled() {
    return enabled;
  }

  /** {@link SentryLogger} properties. */
  @Configuration
  @ConfigurationProperties(prefix = "sentry")
  @Data
  public static class Properties {
    private URI baseUri = URI.create("https://sentry.io/");
    private String organization;
    private String team;
    private String environment = "production";
    private String apiToken;
    private boolean enabled;
    private int eventCap = 100;
    private List<Rule> rules;
  }

  /** {@link SentryLogger} rule properties. */
  @Data
  public static class Rule {
    private List<Action> actions;
    private String actionMatch = "all";
    private String name = "applogs";
    private String owner;
    private int frequency = 30;
    private String environment = "production";
  }

  /** {@link SentryLogger} rule action properties. */
  @Data
  public static class Action {
    private String id;
    private String workspace;
    private String channel;
  }

  /**
   * A circuit breaker which flips off after incrementing to a certain number and resets when a new
   * time period is reached.
   */
  public class Breaker {
    private int count;

    /**
     * Resets the breaker if timestamp in new time period otherwise increments. Returns true if
     * breaker has not been flipped.
     *
     * @param timestamp time of the event
     * @return if breaker has not been flipped
     */
    public boolean increment() {
      return count++ < eventCap;
    }
  }
}
