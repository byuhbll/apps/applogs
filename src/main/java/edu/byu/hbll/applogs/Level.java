package edu.byu.hbll.applogs;

/** Logging level. */
public enum Level {
  FATAL,
  WARN,
  ERROR,
  INFO,
  DEBUG,
  TRACE;

  /**
   * Parses the given log level into a {@link Level}.
   *
   * @param logLevel the log level
   * @return the parsed level
   */
  public static Level parse(String logLevel) {
    if (logLevel == null) {
      return INFO;
    }

    switch (logLevel.toLowerCase()) {
      case "fatal":
      case "critical":
        return FATAL;
      case "error":
        return ERROR;
      case "warn":
      case "warning":
        return WARN;
      case "info":
        return INFO;
      case "debug":
        return DEBUG;
      case "trace":
        return TRACE;
      default:
        return INFO;
    }
  }
}
