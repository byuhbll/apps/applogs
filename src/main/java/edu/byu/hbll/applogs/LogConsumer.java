package edu.byu.hbll.applogs;

import com.fasterxml.jackson.databind.JsonNode;
import edu.byu.hbll.json.ObjectMapperFactory;
import edu.byu.hbll.json.UncheckedObjectMapper;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.format.DateTimeParseException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import javax.annotation.PreDestroy;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

/**
 * Reads Rancher cluster log messages from Kafka and sends them to multiple loggers including the
 * {@link FileLogger} and {@link SentryLogger}.
 */
@Component
@Slf4j
public class LogConsumer {

  private static final UncheckedObjectMapper mapper = ObjectMapperFactory.newUnchecked();

  private final FileLogger fileLogger;
  private final SentryLogger sentryLogger;

  private final Map<String, EventBuffer> buffers = new HashMap<>();
  private final ScheduledThreadPoolExecutor executor = new ScheduledThreadPoolExecutor(1);
  private final Map<String, Stats> stats = new HashMap<>();

  private String prevProjectName = "";

  public LogConsumer(FileLogger fileLogger, SentryLogger sentryLogger) {
    this.fileLogger = fileLogger;
    this.sentryLogger = sentryLogger;
    executor.setRemoveOnCancelPolicy(true);
  }

  /**
   * Listens for log events on Kafka.
   *
   * @param record the kafka record containing the log event
   */
  @KafkaListener(topics = "${spring.kafka.consumer.topic}")
  public void listen(String record) {
    JsonNode node = mapper.readTree(record);
    JsonNode k8s = node.path("kubernetes");

    String rawMessage = node.path("message").asText().replaceFirst("\n$", "");
    String podName = k8s.path("pod_name").asText();
    String simplePodName = podName.replaceFirst("-[^-]+-[^-]+$", "");
    String namespace = k8s.path("namespace_name").asText();
    boolean isApp = k8s.path("container_name").asText().equals("auto-deploy-app");
    String appName = isApp ? namespace.replaceFirst("-\\d+$", "") : null;
    String projectName = isApp ? appName : namespace;
    String branchName =
        isApp && podName.startsWith("review-")
            ? simplePodName.replaceFirst("^review-(.*?)-[^-]+$", "$1")
            : null;

    Event event = new Event();
    event.setJson(node);
    event.setPodName(podName);
    event.setSimplePodName(simplePodName);
    event.setNamespace(namespace);
    event.setAppName(appName);
    event.setBranchName(branchName);
    event.setProjectName(projectName);
    event.setRawMessage(rawMessage);

    try {
      Instant timestamp =
          ZonedDateTime.parse(rawMessage.replaceFirst("(.+?)\\s.*", "$1")).toInstant();
      Level level = Level.parse(rawMessage.replaceFirst(".*?\\s+(\\S+).*", "$1"));
      String message = rawMessage.replaceFirst("\\S+\\s+\\S+\\s+", "");

      event.setTimestamp(timestamp);
      event.setLevel(level);
      event.setMessage(message);
    } catch (DateTimeParseException e) {
      // subsequent line in a multiline log event or non-parseable line
    }

    fileLogger.accept(event);

    // creates a dummy EventBuffer the first time so that buffer is not null
    EventBuffer buffer = buffers.computeIfAbsent(podName, k -> new EventBuffer(event));

    synchronized (buffer) {
      if (buffer.event == event) {
        schedule(buffer);
      } else if (event.isFirstLine() || buffer.isCommitted() || !buffer.isParsed()) {
        buffer.commit();
        EventBuffer newBuffer = buffers.compute(podName, (k, v) -> new EventBuffer(event));
        schedule(newBuffer);
      } else {
        buffer.append(rawMessage);
      }
    }

    getStats(projectName).incrementLineCount();

    if (projectName != null && !projectName.equals(prevProjectName)) {
      getStats(prevProjectName).print();
      prevProjectName = projectName;
    }
  }

  /**
   * Returns the stats object for the given project, creating one if needed.
   *
   * @param projectName the project name
   * @return the corresponding stats object
   */
  private Stats getStats(String projectName) {
    return stats.computeIfAbsent(projectName, k -> new Stats(projectName));
  }

  /**
   * Schedules the given buffer to run ten seconds in the future.
   *
   * @param buffer the buffer to schedule
   */
  private void schedule(EventBuffer buffer) {
    buffer.future = executor.schedule(() -> buffer.commit(), 1000, TimeUnit.MILLISECONDS);
  }

  @PreDestroy
  public void preDestroy() {
    for (Runnable task : executor.shutdownNow()) {
      task.run();
    }
  }

  /** An event wrapper allowing subsequent lines in a multiline event to be appended. */
  private class EventBuffer {

    public static final int MAX_MESSAGE_SIZE = 1024 * 1024;

    private Event event;
    private StringBuilder messageBuilder;
    private ScheduledFuture<?> future;
    private boolean committed;

    /**
     * Creates a new {@link EventBuffer} with given event.
     *
     * @param event the underlying event for this buffer
     */
    public EventBuffer(Event event) {
      this.event = event;
    }

    /**
     * Appends another line to this multiline event.
     *
     * @param message the message to append
     */
    public synchronized void append(String message) {
      if (committed) {
        return;
      }

      if (messageBuilder == null) {
        messageBuilder = new StringBuilder();
      }

      messageBuilder.append("\n");
      messageBuilder.append(message);

      if (messageBuilder.length() >= MAX_MESSAGE_SIZE) {
        commit();
      }
    }

    /**
     * Finalizes the event. Important for multiline events.
     *
     * @return the event
     */
    public synchronized void commit() {
      if (committed) {
        return;
      }

      if (future != null) {
        future.cancel(false);
      }

      if (messageBuilder != null) {
        String remainder = messageBuilder.toString();
        event.setRawMessage(event.getRawMessage() + remainder);
        event.setMessage(event.getMessage() + remainder);
      }

      sentryLogger.accept(event);
      committed = true;
      getStats(event.getProjectName()).incrementEventCount();

      return;
    }

    /**
     * Returns whether or not this buffer has been committed.
     *
     * @return whether or not this buffer has been committed
     */
    public synchronized boolean isCommitted() {
      return committed;
    }

    /**
     * Returns whether or not this buffer's main event was parseable.
     *
     * @return whether or not this buffer's main event was parseable
     */
    public boolean isParsed() {
      return event.getTimestamp() != null;
    }
  }

  /** Holds line and event stats for each project. */
  @Data
  @NoArgsConstructor
  @AllArgsConstructor
  private class Stats {
    private String projectName;
    private long lineCount;
    private long totalLineCount;
    private long totalEventCount;

    /**
     * Creates a new {@link Stats} with the given project name.
     *
     * @param projectName the project name
     */
    public Stats(String projectName) {
      this.projectName = projectName;
    }

    /** Increments the line count. */
    public void incrementLineCount() {
      lineCount++;
      totalLineCount++;
    }

    /** Increments the event count. */
    public void incrementEventCount() {
      totalEventCount++;
    }

    /** Prints the current stats and resets the current line count to zero. */
    public void print() {
      Stats maxStats = getStats("");
      maxStats.lineCount = Math.max(maxStats.lineCount, lineCount);
      maxStats.totalLineCount = Math.max(maxStats.totalLineCount, totalLineCount);
      maxStats.totalEventCount = Math.max(maxStats.totalEventCount, totalEventCount);

      int lineCountPadding = calculatePadding(maxStats.lineCount);
      int totalLineCountPadding = calculatePadding(maxStats.totalLineCount);
      int totalEventCountPadding = calculatePadding(maxStats.totalEventCount);

      log.info(
          "{} li, {} tl, {} ev, {}",
          String.format("%" + lineCountPadding + "s", lineCount),
          String.format("%" + totalLineCountPadding + "s", totalLineCount),
          String.format("%" + totalEventCountPadding + "s", totalEventCount),
          projectName);

      lineCount = 0;
    }

    /**
     * Calculates amount of padding (or number of digits) for the given value.
     *
     * @param value the integer value
     * @return amount of padding
     */
    private int calculatePadding(long value) {
      if (value < 1) {
        return 1;
      } else {
        return (int) (Math.log10(value) + 1);
      }
    }
  }
}
