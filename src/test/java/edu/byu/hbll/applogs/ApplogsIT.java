package edu.byu.hbll.applogs;

import static io.specto.hoverfly.junit.dsl.HoverflyDsl.service;
import static io.specto.hoverfly.junit.verification.HoverflyVerifications.times;
import static org.junit.Assert.assertEquals;

import com.fasterxml.jackson.databind.JsonNode;
import edu.byu.hbll.json.YamlLoader;
import edu.byu.hbll.misc.Resources;
import io.specto.hoverfly.junit.core.Hoverfly;
import io.specto.hoverfly.junit5.HoverflyExtension;
import io.specto.hoverfly.junit5.api.HoverflySimulate;
import java.io.File;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.io.TempDir;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.testcontainers.containers.KafkaContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

/**
 * @author Charles Draper
 */
@SpringBootTest
@ActiveProfiles("test")
@Testcontainers
@ExtendWith(HoverflyExtension.class)
@HoverflySimulate(enableAutoCapture = true)
public class ApplogsIT {

  private static final String DATE = "2022-01-11T11:47:54.523-07:00";

  @Container
  public static KafkaContainer kafka =
      new KafkaContainer(DockerImageName.parse("confluentinc/cp-kafka:5.4.3"))
          .withEnv("KAFKA_AUTO_CREATE_TOPICS_ENABLE", "true");

  @TempDir static File tempDir;

  @BeforeAll
  public static void beforeAll() throws Exception {
    System.setProperty("spring.kafka.bootstrap-servers", kafka.getBootstrapServers());
    System.setProperty("file.baseLogDir", tempDir.getPath());
  }

  @Autowired private KafkaTemplate<String, String> kafkaTemplate;

  @BeforeEach
  public void beforeOnce() throws Exception {
    Thread.sleep(5000);
    produceMessages();
    Thread.sleep(5000);
  }

  private void produceMessages() throws Exception {
    String yaml = Resources.readString("messages.yml", StandardCharsets.UTF_8);
    JsonNode messages = new YamlLoader().load(new StringReader(yaml));

    for (JsonNode message : messages) {
      kafkaTemplate.send("applogs", message.toString());
    }
  }

  @Test
  void test(Hoverfly hoverfly) throws Exception {
    testFileLogger();
    testSentryLogger(hoverfly);
  }

  public void testFileLogger() throws Exception {
    assertEquals(DATE + " INFO message1\n", readFile("test1/test1.log"));
    assertEquals(
        DATE
            + " ERROR message2a\nmessage2b\n"
            + DATE
            + " ERROR many\n\tat edu.byu.hbll\n"
            + DATE
            + " ERROR many\n\tat edu.byu.hbll\n"
            + DATE
            + " ERROR many\n\tat edu.byu.hbll\n",
        readFile("test2/test2.log"));
    assertEquals(DATE + " INFO message3\n", readFile("test3/test3.log"));
    assertEquals(DATE + " INFO message4\n", readFile("test4/branch1/test4.log"));
    assertEquals("message5\n", readFile("ingress-nginx/ingress-nginx.log"));
  }

  public void testSentryLogger(Hoverfly hoverfly) throws Exception {
    hoverfly.verify(service("sentry.io").get("/api/0/projects/test/test2/keys/"), times(2));
    hoverfly.verify(
        service("sentry.io").post("/api/0/teams/test/test/projects/").anyBody(), times(1));
    hoverfly.verify(service("sentry.io").get("/api/0/projects/test/test2/rules/"), times(1));
    hoverfly.verify(
        service("sentry.io").post("/api/0/projects/test/test2/rules/").anyBody(), times(1));
    hoverfly.verify(
        service("o1095719.ingest.sentry.io").post("/api/1234567/envelope/").anyBody(), times(3));
  }

  private String readFile(String file) throws Exception {
    return new String(Files.readAllBytes(new File(tempDir, file).toPath()));
  }
}
