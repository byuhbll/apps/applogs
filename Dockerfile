# build stage
FROM maven:3-ibm-semeru-17-focal as builder
# directory for building the application
WORKDIR /build
# copy pom only first
COPY pom.xml .
# download application dependencies to cache in image layer
RUN mvn dependency:go-offline clean package || true
# copy entire project
COPY . /build
# build the project
RUN mvn -Dmaven.test.skip=true clean package

# run stage
FROM ibm-semeru-runtimes:open-17-jre-focal
# copy binary from build stage
COPY --from=builder /build/target/*.jar /app.jar
# create non-root user
RUN groupadd -g 84602 byuhbll && useradd -u 84602 -g 84602 -s /bin/bash byuhbll
# set to run container as non-root user
USER byuhbll:byuhbll
# use `exec` to make app pid 1, additional java options set using environment variables
ENTRYPOINT exec java $JAVA_OPTS -jar app.jar
