Reads Rancher cluster logging messages from Kafka and prints them to rotating log files. Logs are
rotated daily, unless the logs grow too big at which point they're rotated more often. A new set
of logs are created per Kubernetes namespace and application release.

In addition to file logging, error or fatal messages are sent to Sentry if enabled.

Log messages need to be in the following format to be parsed:

```
ISO_DATE_TIME_OFFSET LEVEL MESSAGE
2022-01-11T11:47:54.523-07:00 ERROR the unique message to log
```

Messages not adhering to this standard will still be printed to the file logs, but Sentry will not pick up any errors.

# Quickstart

Use environment variables to configure the application.

* Spring Kafka
  * **SPRING_KAFKA_BOOTSTRAP_SERVERS**: Kafka bootstrap servers (required)
  * **SPRING_KAFKA_SECURITY_PROTOCOL**: Use `SSL` if secure (optional)
  * **SPRING_KAFKA_CONSUMER_GROUP_ID**: Kafka group ID for this client (required)
  * **SPRING_KAFKA_CONSUMER_TOPIC**: Kafka topic housing the Rancher logs (required)
  * [More properties](https://docs.spring.io/spring-boot/docs/current/reference/html/application-properties.html#application-properties.integration.spring.kafka.bootstrap-servers)
* File Logger
  * **FILE_BASE_LOG_DIR**: Location to write the log files (default: ./applogs)
  * **FILE_MAX_FILE_SIZE**: Maximum individual log file size (default: 100MB)
  * **FILE_TOTAL_SIZE_CAP**: Cap for total size of logs (default: 1G)
  * **FILE_MAX_HISTORY**: Number of days of logs (default: 90)
  * **FILE_ENABLED**: Whether or not the file logger is enabled (default: true)
* Sentry Logger
  * **SENTRY_BASE_URI**: The base URI of the Sentry instance (default: https://sentry.io/)
  * **SENTRY_ORGANIZATION**: The Sentry organization (required)
  * **SENTRY_TEAM**: The Sentry team (required)
  * **SENTRY_ENVIRONMENT**: The environment (default: production)
  * **SENTRY_API_TOKEN**: The API token for the Sentry web services (required)
  * **SENTRY_ENABLED**: Whether or not the Sentry logger is enabled (default: false)
  * **SENTRY_EVENT_CAP**: Max number of events to send per period per error (default: 100)
  * **SENTRY_CAP_RESET_SCHEDULE**: Schedule to reset event counter (default: 0 0 * * * * * (hourly))

## Docker

```
docker run --name applogs --env-file /etc/applogs/container.env -v /var/log/apps:./applogs registry.gitlab.com/byuhbll/app/applogs
```

## Java
```
source /etc/applogs/container.env
java -jar target/applogs.jar
```
